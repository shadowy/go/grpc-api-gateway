package grpc_api_gateway

import (
	"errors"
	"strings"
)

func parseGRPCPath(path string) (service string, method string, err error) {
	parts := strings.Split(path, "/")
	l := len(parts)
	if l < 2 {
		return "", "", errors.New("gRPC path not correct")
	}
	service = parts[l-2]
	method = parts[l-1]
	return
}
