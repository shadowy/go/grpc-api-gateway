package main

import (
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/grpc-api-gateway"
	"gitlab.com/shadowy/go/grpc-api-gateway/settings"
	"net"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	logrus.Info("Example - Api Gateway")
	lis, err := net.Listen("tcp", ":5050")
	if err != nil {
		logrus.WithError(err).Error("Example - Api Gateway: Listen")
		return
	}

	host := "consul://Ping/"
	cfg := settings.APIGateway{
		Services: []*settings.APIService{
			&settings.APIService{
				Service: "test.Ping",
				Host:    &host,
			},
		},
	}
	cfgConsul := api.DefaultConfig()
	cfgConsul.Address = "cfg.local"
	cfgConsul.Scheme = "http"
	//	cfg.Token = "FBAF54CC-E03D-4763-9F19-376114D3857B"
	//client, err := api.NewClient(cfgConsul)

	//consul.InitConsulResolver(client)
	server, _, err := grpc_api_gateway.CreateApiGateway(&cfg, nil)
	if err != nil {
		logrus.WithError(err).Error("Example - Api Gateway: CreateApiGateway")
		return
	}

	logrus.Info("Example - Api Gateway: start")
	err = server.Serve(lis)
	if err != nil {
		logrus.WithError(err).Error("Example - Api Gateway: start")
		return
	}
}
