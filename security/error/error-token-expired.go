package error

type ErrorTokenExpired struct{}

func (e *ErrorTokenExpired) Error() string {
	return "Token expired"
}
