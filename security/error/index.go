package error

type securityServiceError struct{}

var SecurityServiceError = new(securityServiceError)

func (s *securityServiceError) UserNotFound() error {
	return &ErrorUserNotFound{}
}

func (s *securityServiceError) TokenExpired() error {
	return &ErrorTokenExpired{}
}
