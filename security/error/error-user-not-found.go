package error

type ErrorUserNotFound struct{}

func (e *ErrorUserNotFound) Error() string {
	return "User not found"
}
