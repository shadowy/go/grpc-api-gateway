package security

import "google.golang.org/grpc/metadata"

type Service interface {
	GetUserInfoByToken(token *string) (userID, permissions *string, err error)
	ExtraValidation(token *string, md metadata.MD, service, method *string) error
}
