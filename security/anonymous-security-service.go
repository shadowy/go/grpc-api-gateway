package security

import "google.golang.org/grpc/metadata"

type AnonymousSecurityService struct{}

func (s *AnonymousSecurityService) GetUserInfoByToken(token *string) (userID *string, permissions *string, err error) {
	return
}

func (s *AnonymousSecurityService) ExtraValidation(token *string, md metadata.MD, service, method *string) error {
	return nil
}
