package grpc_api_gateway

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/grpc-api-gateway/log"
	"gitlab.com/shadowy/go/grpc-api-gateway/proxy"
	"gitlab.com/shadowy/go/grpc-api-gateway/security"
	"gitlab.com/shadowy/go/grpc-api-gateway/settings"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type RefreshSetting func(cfg *settings.APIGateway) error

func CreateApiGateway(settings *settings.APIGateway, securityService security.Service) (
	*grpc.Server, RefreshSetting, error) {

	logrus.Info("grpc-api-gateway.CreateApiGateway")
	log.InitLogrus()
	if securityService == nil {
		logrus.Warn("grpc-api-gateway.CreateApiGateway security is not defined")
		securityService = &security.AnonymousSecurityService{}
	}
	director, refresh, err := createDirector(settings, securityService)
	if err != nil {
		return nil, nil, err
	}
	server := grpc.NewServer(grpc.CustomCodec(proxy.Codec()), grpc.UnknownServiceHandler(proxy.TransparentHandler(director)))
	return server, refresh, nil
}

func createDirector(cfg *settings.APIGateway, securityService security.Service) (
	proxy.StreamDirector, RefreshSetting, error) {
	logrus.Info("grpc-api-gateway.createDirector")
	connections := *getServicesMap(cfg)

	refresh := func(cfg *settings.APIGateway) error {
		logrus.Info("grpc-api-gateway.createDirector.refresh")
		connections = *getServicesMap(cfg)
		return nil
	}

	director := func(ctx context.Context, fullMethodName string) (context.Context, *grpc.ClientConn, error) {
		service, method, err := parseGRPCPath(fullMethodName)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{"method": fullMethodName}).
				Error("grpc-api-gateway.createDirector director")
			return nil, nil, status.Errorf(codes.Internal, "Can't parse path: %s", fullMethodName)
		}
		logrus.WithFields(logrus.Fields{"method": method, "service": service}).
			Debug("grpc-api-gateway.createDirector director")
		if srv, ok := connections[service]; ok {
			md, ok := metadata.FromIncomingContext(ctx)
			if !ok {
				return nil, nil, status.Errorf(codes.Internal, "Can't get incoming context %s", fullMethodName)
			}
			con, err := getConnection(ctx, srv)
			if err != nil {
				return nil, nil, status.Error(codes.Internal, err.Error())
			}
			outCtx, _ := context.WithCancel(ctx)
			md = md.Copy()
			authorizations := md.Get("Authorization")
			if len(authorizations) > 0 {
				userId, permissions, err := securityService.GetUserInfoByToken(&authorizations[0])
				if err != nil {
					return nil, nil, status.Errorf(codes.Internal, "Can't get userId and permissions: %s", err.Error())
				}
				md.Set("sec-user-id", *userId)
				md.Set("sec-permissions", *permissions)
				err = securityService.ExtraValidation(&authorizations[0], md, &service, &method)
				if err != nil {
					return nil, nil, status.Errorf(codes.Internal, "Can't proceed extra validation: %s", err.Error())
				}
			} else {
				delete(md, "sec-user-id")
				delete(md, "sec-permissions")
			}
			outCtx = metadata.NewOutgoingContext(outCtx, md)
			return outCtx, con, nil
		} else {
			return nil, nil, status.Errorf(codes.Unimplemented, "Service %s not found", service)
		}
	}

	return director, refresh, nil
}

func getServicesMap(cfg *settings.APIGateway) *map[string]*settings.APIService {
	result := make(map[string]*settings.APIService)
	for i := range cfg.Services {
		srv := cfg.Services[i]
		result[srv.Service] = srv
	}
	return &result
}

func getConnection(ctx context.Context, service *settings.APIService) (*grpc.ClientConn, error) {
	logrus.WithFields(service.Fields()).Debug("grpc-api-gateway.getConnection")
	conn, err := grpc.DialContext(ctx, *service.Host, grpc.WithInsecure(), grpc.WithCodec(proxy.Codec()))
	if err != nil {
		logrus.WithFields(service.Fields()).WithError(err).Error("grpc-api-gateway.getConnection")
		return nil, err
	}
	go closeConnection(ctx, conn)
	return conn, nil
}

func closeConnection(ctx context.Context, connection *grpc.ClientConn) {
	for {
		select {
		case <-ctx.Done():
			logrus.Debug("grpc-api-gateway.closeConnection")
			_ = connection.Close()
			return
		}
	}
}
