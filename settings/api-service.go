package settings

import "github.com/sirupsen/logrus"

type APIService struct {
	Service    string  `yaml:"service"`
	Host       *string `yaml:"host"`
	ConsulName *string `yaml:"consulName"`
}

func (s *APIService) Fields() logrus.Fields {
	return logrus.Fields{"service": s.Service, "host": nilString(s.Host), "consulName": nilString(s.ConsulName)}
}

func nilString(data *string) string {
	if data == nil {
		return ""
	}
	return *data
}
