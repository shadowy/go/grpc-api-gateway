package settings

type APIGateway struct {
	Services []*APIService `yaml:"services"`
}
