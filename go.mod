module gitlab.com/shadowy/go/grpc-api-gateway

go 1.15

require (
	github.com/golang/protobuf v1.4.2
	github.com/hashicorp/consul v1.9.1 // indirect
	github.com/hashicorp/consul/api v1.8.1
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20200930145003-4acb6c075d10
	google.golang.org/grpc v1.34.0
)
