package log

import (
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/grpclog"
)

type logrusGrpcLoggerV2 struct {
	*logrus.Entry
}

func (l *logrusGrpcLoggerV2) V(level int) bool {
	return l.Logger.IsLevelEnabled(logrus.Level(level))
}

func InitLogrus() {
	grpclog.SetLoggerV2(&logrusGrpcLoggerV2{logrus.WithFields(logrus.Fields{})})
}
