all: test lint-full

test:
	@echo ">> test"

lint-full: lint card

card:
	@echo ">> card"
	#@goreportcard-cli -v

lint:
	@echo ">> lint"
	#@golangci-lint run
